Prism.languages.bamboo = {
	punctuation: /[\{\}\,]/,
	string: {
		pattern: /(".*")/,
		greedy: !0,
	},
	function: {
		pattern: /[\#\!](("?.*"?)|([^"][a-zA-Z_\-0-9]*[^"]))/,
	},
	number: /\d+/,
	comment: {
		pattern: /(^|[^\\:]);.*/,
		greedy: !0,
	},
};
